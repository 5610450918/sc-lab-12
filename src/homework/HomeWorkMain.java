package homework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.Box.Filler;

public class HomeWorkMain {
	private FileReader fileReader;
	private FileWriter fileWriter;
//	private	BufferedWriter out;
	private ArrayList<HomeWork> homework ;
	public static void main(String[] args){
		try{
		HomeWorkMain hw = new HomeWorkMain();
//		System.out.println(hw.allScore());
		FileWriter fileWriter = new FileWriter("average.txt");
//		BufferedWriter out = new BufferedWriter(fileWriter);
//		out.write(hw.allScore());
		PrintWriter out = new PrintWriter(fileWriter);
		out.println(hw.allScore());
		out.flush();
		FileReader file = new FileReader("average.txt");
		BufferedReader buffer = new BufferedReader(file);
		for(String line=buffer.readLine();line!=null;line=buffer.readLine()){
			System.out.println(line);
		}
		}catch(IOException e){
			
		}
	}
	
	public HomeWorkMain(){
		try {
			homework = new ArrayList<HomeWork>();
			fileWriter = new FileWriter("average.txt",true);
//			out = new BufferedWriter(fileWriter);
			fileReader = new FileReader("homework.txt");
			BufferedReader buffer = new BufferedReader(fileReader);
			for(String line=buffer.readLine();line!=null;line=buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0];
				HomeWork hw = new HomeWork(name);
				for(int i=1;i<=5;i++){
					hw.addScore(Double.parseDouble(data[i]));
				}
				addHomeWork(hw);
				
				
			}
			buffer.close();
//			writeDetailInFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}
	public void addHomeWork(HomeWork h){
		homework.add(h);
	}
/**	public void writeDetailInFile() throws IOException{
		FileWriter fileWriter = new FileWriter("average.txt");
		PrintWriter out = new PrintWriter(fileWriter);
		out.println("-------------------------- HomeWork Scores --------------------------\n");
		out.println("Name\tScore");
		out.println("====================");
		for(HomeWork h : homework){
			out.println(h.tosString());
		}
		out.flush();
		
	}**/
	public String allScore(){
		String str = "------------------------- HomeWork Score -----------------------------\n";
		str += "Name\tScore\n";
		str += "==========================\n";
		for(HomeWork h : homework){
			str += h.tosString()+"\n";
		}
		return str;
	}
	
}
