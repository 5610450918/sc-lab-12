package homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class HomeWorkExamMain {
	private ArrayList<HomeWork> homeworks;
	private FileReader filereader;
	public static void main(String[] agrs){
		try{
			HomeWorkExamMain hw = new HomeWorkExamMain();
			FileWriter filewriter = new FileWriter("average.txt",true);
			PrintWriter out = new PrintWriter(filewriter);
			out.println(hw.allScoreExam());
			out.flush();
			FileReader file = new FileReader("average.txt");
			BufferedReader buffer = new BufferedReader(file);
			for(String line=buffer.readLine();line!=null;line=buffer.readLine()){
				System.out.println(line);
			}
		}catch(IOException e){
			
		}
	}
	public HomeWorkExamMain(){
		try{
			homeworks = new ArrayList<HomeWork>();
			filereader = new FileReader("exam.txt");
			BufferedReader in = new BufferedReader(filereader);
			for(String line=in.readLine();line!=null;line=in.readLine()){
				String[] data = line.split(", ");
				String name = data[0];
				HomeWork hw = new HomeWork(name);
				for(int i=1;i<=2;i++){
					hw.addScore(Double.parseDouble(data[i]));
				}
				addExam(hw);
			}
			in.close();
		}catch(IOException e){
			
		}
	}
	public void addExam(HomeWork h){
		homeworks.add(h);
		
	}
	public String allScoreExam(){
		String str = "--------------------------- Exam Scores --------------------------\n";
		str += "Name\tExam\n";
		str += "=======================\n";
		for(HomeWork h : homeworks){
			str += h.tosString()+"\n";
		}
		return str;
	}
}
