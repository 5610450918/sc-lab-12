package homework;

import java.util.ArrayList;

public class HomeWork {
	private String name;
	private ArrayList<Double> score ;
	public HomeWork(String name){
		this.name = name;
		score = new ArrayList<Double>();
	}
	public double getAverage(){
		double score = 0;
		for(Double points : this.score){
			score += points;	
		}
		return score/this.score.size();
	}
	public String tosString(){
		return name+"\t"+getAverage();
	}
	public void addScore(double point){
		score.add(point);
	}
}
