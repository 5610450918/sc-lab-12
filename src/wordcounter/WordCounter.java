package wordcounter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WordCounter {
	private String message;
	private FileReader fileReader;
	private HashMap<String, Integer> wordCount;
	public WordCounter(String file){
		try {
			message = "";
			wordCount = new HashMap<String,Integer>();
			fileReader = new FileReader(file);
			BufferedReader in = new BufferedReader(fileReader);
			for(String line=in.readLine();line!=null;line=in.readLine()){
				this.message += line+" ";
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
		}
		
	}
	public void count(){
//		System.out.println(message);
		for(String str : message.split(" ")){
			if(wordCount.containsKey(str)==true){
				wordCount.put(str, wordCount.get(str)+1);
			}
			else{
				wordCount.put(str, 1);
			}
		}
		
	}
	
	public String getCountData(){
		String str = "";
		for(Map.Entry<String,Integer> entry : wordCount.entrySet()){
			str += entry+"\n";
		}
		return str;
	}
}
