package wordcounter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class WordCounterMain {
	public static void main(String[] args)throws IOException{
		WordCounter wordCount = new WordCounter("poem.txt");
		wordCount.count();
		System.out.println(wordCount.getCountData());
	}
}
