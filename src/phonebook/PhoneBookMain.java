package phonebook;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PhoneBookMain {
	public static void main(String[] args){
		try{
			PhoneBookMain phonebook = new PhoneBookMain();
			FileReader fileReader = new FileReader("phonebook.txt");
			BufferedReader buffer = new BufferedReader(fileReader);
			for(String line=buffer.readLine();line!=null;line=buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0];
				String phone = data[1];
				phonebook.addPhone(new PhoneBook(name, phone));
			}
			System.out.println(phonebook.allDetailPhone());
			
		}
		catch(IOException e){
			
		}
	}
	private ArrayList<PhoneBook> phoneBooks;
	public PhoneBookMain(){
		phoneBooks = new ArrayList<PhoneBook>();
	}
	public void addPhone(PhoneBook p){
		phoneBooks.add(p);
	}
	public String allDetailPhone(){
		String str = "------------------------- Phone Book ---------------------------\n";
		str += "Name\tPhone\n";
		str += "\n===================\n";
		for(PhoneBook p : phoneBooks){
			str += p.toString()+"\n";
		}
		return str;
	}

}
